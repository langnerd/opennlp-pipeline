package com.langnerd.nlp.server.processors.impl

import com.langnerd.nlp.api.processors.{SentenceDetector => ApiSentenceDetector}
import com.langnerd.nlp.server.model.Sentence
import com.langnerd.nlp.server.processors.SentenceDetector

class DefaultSentenceDetector(api: ApiSentenceDetector) extends SentenceDetector {

  override def detect(text: String): List[Sentence] =
    api.detect(text).map(Sentence(_, List.empty, List.empty))
}

object DefaultSentenceDetector {
  def apply(api: ApiSentenceDetector): DefaultSentenceDetector = new DefaultSentenceDetector(api)
}
