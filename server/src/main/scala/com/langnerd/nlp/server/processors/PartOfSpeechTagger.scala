package com.langnerd.nlp.server.processors

import com.langnerd.nlp.server.model.Token

trait PartOfSpeechTagger {
  def tag(tokens: List[Token]): List[Token]
}

