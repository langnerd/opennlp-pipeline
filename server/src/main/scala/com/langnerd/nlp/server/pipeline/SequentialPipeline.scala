package com.langnerd.nlp.server.pipeline

import com.langnerd.nlp.server.model.Sentence
import com.langnerd.nlp.server.processors.{Lemmatizer, PartOfSpeechTagger, SentenceDetector, Tokenizer}

class SequentialPipeline private (sentenceDetector: SentenceDetector,
                                  tokenizer: Tokenizer,
                                  tagger: PartOfSpeechTagger,
                                  lemmatizer: Lemmatizer) {

  def process(text: String): List[Sentence] = {
    sentenceDetector.detect(text).map { sentence =>
      sentence.copy(tokens = lemmatizer.lemmatize(tagger.tag(tokenizer.tokenize(sentence.text))))
    }
  }
}

object SequentialPipeline {
  def apply(sentenceDetector: SentenceDetector,
            tokenizer: Tokenizer,
            tagger: PartOfSpeechTagger,
            lemmatizer: Lemmatizer): SequentialPipeline =
    new SequentialPipeline(sentenceDetector, tokenizer, tagger, lemmatizer)
}
