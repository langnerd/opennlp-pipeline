package com.langnerd.nlp.server.model

import NamedEntity.NamedEntity

final case class Span(start: Int, end: Int, namedEntity: NamedEntity)
