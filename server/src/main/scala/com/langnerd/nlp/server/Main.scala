package com.langnerd.nlp.server

import java.util.concurrent.Executors

import cats.effect.{ContextShift, ExitCode, IO, IOApp, Resource}
import cats.syntax.flatMap._
import com.langnerd.nlp.api.processors.opennlp.OpenNLPProcessorFactory
import com.langnerd.nlp.server.pipeline.{Pipeline, PipelineBuilder, PipelineModule}

import scala.concurrent.{ExecutionContext, ExecutionContextExecutorService}
import scala.util.{Failure, Success, Try}

object Main extends IOApp {

  // TODO read all of the below from config
  private final val ResourceRoot =
    "/Users/zezulat/IdeaProjects/opennlp-pipeline/server/src/main/resources"

  // Config
  private final val SentenceDetectorModelEn   = s"$ResourceRoot/models/en-sent.bin"
  private final val TokenizerModelEn          = s"$ResourceRoot/models/en-token.bin"
  private final val PartOfSpeechTaggerModelEn = s"$ResourceRoot/models/en-pos-maxent.bin"
  private final val LemmatizerModelEn         = s"$ResourceRoot/models/en-lemmatizer.dict"
  private final val NerModelDateEn            = s"$ResourceRoot/models/en-ner-date.bin"
  private final val NerModelLocationEn        = s"$ResourceRoot/models/en-ner-location.bin"
  private final val NerModelMoneyEn           = s"$ResourceRoot/models/en-ner-money.bin"
  private final val NerModelOrganizationEn    = s"$ResourceRoot/models/en-ner-organization.bin"
  private final val NerModelPercentageEn      = s"$ResourceRoot/models/en-ner-percentage.bin"
  private final val NerModelPersonEn          = s"$ResourceRoot/models/en-ner-person.bin"
  private final val NerModelTimeEn            = s"$ResourceRoot/models/en-ner-time.bin"
  private final val StopWordsEn               = s"$ResourceRoot/en-stop-words.txt"

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val executionContext: ExecutionContext = ExecutionContext.global
    implicit val contextShift: ContextShift[IO]     = IO.contextShift(executionContext)
    implicit val readExecutionContext: Resource[IO, ExecutionContextExecutorService] =
      Resource.make(IO(ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))))(ec =>
        IO(ec.shutdown()))

    val module = PipelineModule.make[IO](new OpenNLPProcessorFactory)

    // Input
    val paragraph = "London is the capital of and largest city in England " +
      "and the United Kingdom, with the largest municipal population in the European Union." +
      " Standing on the River Thames in the south-east of England, at the head of its 50-mile " +
      "(80 km) estuary leading to the North Sea, London has been a major settlement for two millennia."

    for {
      sentenceDetector     <- processor(SentenceDetectorModelEn, module.sentenceDetector)
      tokenizer            <- processor(TokenizerModelEn, module.tokenizer)
      tagger               <- processor(PartOfSpeechTaggerModelEn, module.tagger)
      lemmatizer           <- processor(LemmatizerModelEn, module.lemmatizer)
      dateResolver         <- processor(NerModelDateEn, module.spanResolver)
      locationResolver     <- processor(NerModelLocationEn, module.spanResolver)
      moneyResolver        <- processor(NerModelMoneyEn, module.spanResolver)
      organizationResolver <- processor(NerModelOrganizationEn, module.spanResolver)
      percentageResolver   <- processor(NerModelPercentageEn, module.spanResolver)
      personResolver       <- processor(NerModelPersonEn, module.spanResolver)
      timeResolver         <- processor(NerModelTimeEn, module.spanResolver)
      stopWords            <- processor(StopWordsEn, module.stopWords)
      pipeline <- pipeline(
        Pipeline.Builder
          .withSentenceDetector(sentenceDetector)
          .withTokenizer(tokenizer)
          .withLemmatizer(lemmatizer)
          .withTagger(tagger)
          .withStopWords(stopWords)
          .withSpanResolver(dateResolver)
          .withSpanResolver(locationResolver)
          .withSpanResolver(moneyResolver)
          .withSpanResolver(organizationResolver)
          .withSpanResolver(percentageResolver)
          .withSpanResolver(personResolver)
          .withSpanResolver(timeResolver))
      doc <- pipeline.process[IO](paragraph)
    } yield {
      pprint.pprintln(doc, height = 200)

      ExitCode.Success
    }
  }

  private def pipeline(builder: PipelineBuilder): IO[Pipeline] = builder.build() match {
    case Success(pipeline) => IO.pure(pipeline)
    case Failure(e) =>
      IO(println(e.getMessage)) >> IO.raiseError(e)
  }
  private def processor[P](modelPath: String, f: String => IO[Try[P]]): IO[P] =
    f(modelPath).flatMap {
      case Success(p) => IO.pure(p)
      case Failure(e) =>
        val message = s"Failed to load model '$modelPath': ${e.getMessage}"
        IO(println(message)) >> IO.raiseError(new IllegalStateException(message))
    }
}
