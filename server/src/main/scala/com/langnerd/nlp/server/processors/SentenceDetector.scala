package com.langnerd.nlp.server.processors

import com.langnerd.nlp.server.model.Sentence

trait SentenceDetector {
  def detect(text: String): List[Sentence]
}
