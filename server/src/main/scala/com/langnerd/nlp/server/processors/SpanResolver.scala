package com.langnerd.nlp.server.processors

import com.langnerd.nlp.server.model.{Span, Token}

trait SpanResolver {
  def resolve(tokens: List[Token]): List[Span]
}

