package com.langnerd.nlp.server.model

final case class Doc(sentences: List[Sentence])

object Doc {
  final val Empty = Doc(List.empty)
}
