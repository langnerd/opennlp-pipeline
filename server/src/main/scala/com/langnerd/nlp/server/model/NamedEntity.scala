package com.langnerd.nlp.server.model

object NamedEntity extends Enumeration {
  type NamedEntity = Value
  val Date, Location, Money, Organization, Percentage, Person, Time = Value

  private val keyMap: Map[String, Value] = Map(
    "DATE"         -> Date,
    "LOCATION"     -> Location,
    "MONEY"        -> Money,
    "ORGANIZATION" -> Organization,
    "PERCENTAGE"   -> Percentage,
    "PERSON"       -> Person,
    "TIME"         -> Time
  )

  def fromName(name: String): Option[NamedEntity] = keyMap.get(name.trim.toUpperCase)
}
