package com.langnerd.nlp.server.model

final case class Token(text: String, tag: Option[Tag.Tag], lemma: Option[Lemma])
