package com.langnerd.nlp.server.processors

import com.langnerd.nlp.server.model.Token

trait Lemmatizer {
  def lemmatize(tokens: List[Token]): List[Token]
}
