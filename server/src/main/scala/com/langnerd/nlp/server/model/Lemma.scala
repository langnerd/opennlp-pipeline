package com.langnerd.nlp.server.model

final case class Lemma(value: String)
