package com.langnerd.nlp.server.processors.impl

import com.langnerd.nlp.api.processors.{SpanResolver => ApiSpanResolver}
import com.langnerd.nlp.server.model.{NamedEntity, Span, Token}
import com.langnerd.nlp.server.processors.SpanResolver

class DefaultSpanResolver(api: ApiSpanResolver) extends SpanResolver {
  override def resolve(tokens: List[Token]): List[Span] = {
    api.resolve(tokens.map(_.text)).flatMap { span =>
      NamedEntity.fromName(span.kind) match {
        case Some(namedEntity) =>
          Some(Span(span.start, span.end, namedEntity))
        case None => None
      }
    }
  }
}

object DefaultSpanResolver {
  def apply(api: ApiSpanResolver): DefaultSpanResolver = new DefaultSpanResolver(api)
}
