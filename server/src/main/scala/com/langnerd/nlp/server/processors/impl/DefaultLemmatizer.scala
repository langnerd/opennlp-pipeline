package com.langnerd.nlp.server.processors.impl

import com.langnerd.nlp.api.processors.{Lemmatizer => ApiLemmatizer}
import com.langnerd.nlp.server.model.{Lemma, Tag, Token}
import com.langnerd.nlp.server.processors.Lemmatizer

import scala.annotation.tailrec

class DefaultLemmatizer(api: ApiLemmatizer) extends Lemmatizer {

  override def lemmatize(tokens: List[Token]): List[Token] = {
    val taggedTokens = tokens.filter(_.tag.isDefined)
    val tags         = taggedTokens.flatMap(_.tag.map(Tag.fromValue))
    val lemmas       = api.lemmatize(taggedTokens.map(_.text), tags)

    lemmatizeTokens(tokens, lemmas)(List.empty)
  }

  @tailrec
  private def lemmatizeTokens(tokens: List[Token], lemmas: List[String])(
      acc: List[Token]): List[Token] = tokens match {
    case x :: xs =>
      x.tag match {
        case Some(_) =>
          lemmas match {
            case y :: ys => lemmatizeTokens(xs, ys)(acc :+ x.copy(lemma = Some(Lemma(y))))
            case Nil     => acc ++ xs
          }
        case None => lemmatizeTokens(xs, lemmas)(acc :+ x)
      }
    case Nil => acc
  }
}

object DefaultLemmatizer {
  def apply(api: ApiLemmatizer): DefaultLemmatizer = new DefaultLemmatizer(api)
}
