package com.langnerd.nlp.server.model

object Tag extends Enumeration {
  type Tag = Value

  // http://paula.petcu.tm.ro/init/default/post/opennlp-part-of-speech-tags
  val Adjective, AdjectiveComparative, AdjectiveSuperlative, Adverb, AdverbComparative,
  AdverbSuperlative, ExistentialThere, ForeignWord, Interjection, ListItemMarker, Modal, NounSingular,
  NounPlural, Particle, PersonalPronoun, PossessiveEnding, PossessivePronoun, PossessiveWhPronoun,
  Predeterminer, Preposition, ProperNounSingular, ProperNounPlural, Symbol, To, VerbBaseForm,
  VerbGerundOrPastParticiple, VerbNonThirdPersonSingularPresent, VerbPastParticiple, VerbPastTense,
  VerbThirdPersonSingularPresent, WhAdverb, WhDeterminer, WhPronoun = Value

  private val keyMap: Map[String, Value] = Map(
    "JJ" -> Adjective,
    "JJR" -> AdjectiveComparative,
    "JJS" -> AdjectiveSuperlative,
    "RB" -> Adverb,
    "RBR" -> AdverbComparative,
    "RBS" -> AdverbSuperlative,
    "EX" -> ExistentialThere,
    "FW" -> ForeignWord,
    "UH" -> Interjection,
    "LS" -> ListItemMarker,
    "MD" -> Modal,
    "NN" -> NounSingular,
    "NNS" -> NounPlural,
    "RP" -> Particle,
    "PRP" -> PersonalPronoun,
    "POS" -> PossessiveEnding,
    "PRP$" -> PossessivePronoun,
    "y" -> PossessiveWhPronoun,
    "y" -> Predeterminer,
    "y" -> Preposition,
    "NNP" -> ProperNounSingular,
    "NNPS" -> ProperNounPlural,
    "SYM" -> Symbol,
    "TO" -> To,
    "VB" -> VerbBaseForm,
    "VBG" -> VerbGerundOrPastParticiple,
    "VBP" -> VerbNonThirdPersonSingularPresent,
    "VBN" -> VerbPastParticiple,
    "VBD" -> VerbPastTense,
    "VBZ" -> VerbThirdPersonSingularPresent,
    "WRB" -> WhAdverb,
    "WDT" -> WhDeterminer,
    "WP" -> WhPronoun
  )

  def fromName(name: String): Option[Value] = {
    keyMap.get(name)
  }

  def fromValue(value: Tag): String = {
    keyMap.find(_._2.equals(value)).map(_._1).get
  }
}
