package com.langnerd.nlp.server.pipeline

import cats.Parallel
import cats.data.NonEmptyList
import cats.effect.{ContextShift, Sync}
import cats.syntax.all._
import com.langnerd.nlp.server.model.{Doc, Sentence, Span}
import com.langnerd.nlp.server.processors._

final case class Pipeline(sentenceDetector: SentenceDetector,
                          tokenizer: Tokenizer,
                          tagger: PartOfSpeechTagger,
                          lemmatizer: Lemmatizer,
                          spanResolvers: List[SpanResolver],
                          stopWords: Set[String]) {

  def process[F[_]](
      text: String)(implicit F: Sync[F], P: Parallel[F], cs: ContextShift[F]): F[Doc] = {
    for {
      tokenized <- tokenize(text, sentenceDetector, tokenizer, tagger, lemmatizer)
      enriched  <- enrich(tokenized, spanResolvers)
    } yield Doc(enriched.toList)
  }

  private def tokenize[F[_]](text: String,
                             sentenceDetector: SentenceDetector,
                             tokenizer: Tokenizer,
                             tagger: PartOfSpeechTagger,
                             lemmatizer: Lemmatizer)(implicit F: Sync[F]): F[List[Sentence]] = {
    F.delay(
      sentenceDetector.detect(text).map { sentence =>
        val tokens = lemmatizer.lemmatize(tagger.tag(
          tokenizer.tokenize(sentence.text).filterNot(t => stopWords.contains(t.text.toLowerCase))))
        sentence.copy(tokens = tokens)
      }
    )
  }

  private def enrich[F[_]](sentences: List[Sentence], spanResolvers: List[SpanResolver])(
      implicit F: Sync[F],
      P: Parallel[F],
      cs: ContextShift[F]): F[NonEmptyList[Sentence]] = {
    val enriched = sentences.map { sentence =>
      for {
        spans <- resolveSpans(sentence, spanResolvers)
      } yield sentence.copy(spans = spans.toList)
    }
    NonEmptyList.of(enriched.head, enriched.tail: _*).parSequence
  }

  private def resolveSpans[F[_]](sentence: Sentence, spanResolvers: List[SpanResolver])(
      implicit F: Sync[F],
      P: Parallel[F],
      cs: ContextShift[F]): F[NonEmptyList[Span]] = {
    val enriched = spanResolvers.flatMap(resolver => resolver.resolve(sentence.tokens)).map(F.pure)
    NonEmptyList.of(enriched.head, enriched.tail: _*).parSequence
  }
}

object Pipeline {
  final val Builder = PipelineBuilder.Empty
}
