package com.langnerd.nlp.server.processors.impl

import com.langnerd.nlp.api.processors.{PartOfSpeechTagger => ApiPartOfSpeechTagger}
import com.langnerd.nlp.server.model.{Tag, Token}
import com.langnerd.nlp.server.processors.PartOfSpeechTagger

import scala.annotation.tailrec

class DefaultPartOfSpeechTagger(api: ApiPartOfSpeechTagger) extends PartOfSpeechTagger {

  override def tag(tokens: List[Token]): List[Token] = {
    val tags = api.tag(tokens.map(_.text))
    tagTokens(tokens, tags)(List.empty)
  }

  @tailrec
  private def tagTokens(tokens: List[Token], tags: List[String])(acc: List[Token]): List[Token] =
    tokens match {
      case x :: xs =>
        tags match {
          case y :: ys =>
            val token = Tag.fromName(y) match {
              case tag @ Some(_) => x.copy(tag = tag)
              case None          => x
            }
            tagTokens(xs, ys)(acc :+ token)
          case Nil => acc
        }
      case Nil => acc
    }
}

object DefaultPartOfSpeechTagger {
  def apply(api: ApiPartOfSpeechTagger): DefaultPartOfSpeechTagger =
    new DefaultPartOfSpeechTagger(api)
}
