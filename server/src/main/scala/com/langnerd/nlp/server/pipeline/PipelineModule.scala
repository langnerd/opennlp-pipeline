package com.langnerd.nlp.server.pipeline

import cats.effect.{ConcurrentEffect, ContextShift, Resource, Sync}
import com.langnerd.nlp.api.processors.ProcessorFactory
import com.langnerd.nlp.server.processors._
import com.langnerd.nlp.server.processors.impl.{
  DefaultLemmatizer,
  DefaultPartOfSpeechTagger,
  DefaultSentenceDetector,
  DefaultSpanResolver,
  DefaultTokenizer
}

import scala.concurrent.ExecutionContextExecutorService
import scala.util.{Failure, Success, Try}

trait PipelineModule[F[_]] {

  def lemmatizer(modelPath: String): F[Try[Lemmatizer]]

  def sentenceDetector(modelPath: String): F[Try[SentenceDetector]]

  def spanResolver(modelPath: String): F[Try[SpanResolver]]

  def tagger(modelPath: String): F[Try[PartOfSpeechTagger]]

  def tokenizer(modelPath: String): F[Try[Tokenizer]]

  def stopWords(path: String): F[Try[Set[String]]]
}

object PipelineModule {

  def make[F[_]: Sync](processorFactory: ProcessorFactory[F])(
      implicit readEC: Resource[F, ExecutionContextExecutorService],
      contextShift: ContextShift[F],
      F: Sync[F],
      ce: ConcurrentEffect[F]): PipelineModule[F] = new PipelineModule[F] {

    private[this] val F = Sync[F]

    override def lemmatizer(modelPath: String): F[Try[Lemmatizer]] =
      processor(modelPath)(processorFactory.lemmatizer, DefaultLemmatizer.apply)

    override def sentenceDetector(modelPath: String): F[Try[SentenceDetector]] =
      processor(modelPath)(processorFactory.sentenceDetector, DefaultSentenceDetector.apply)

    override def spanResolver(modelPath: String): F[Try[SpanResolver]] =
      processor(modelPath)(processorFactory.spanResolver, DefaultSpanResolver.apply)

    override def tagger(modelPath: String): F[Try[PartOfSpeechTagger]] =
      processor(modelPath)(processorFactory.tagger, DefaultPartOfSpeechTagger.apply)

    override def tokenizer(modelPath: String): F[Try[Tokenizer]] =
      processor(modelPath)(processorFactory.tokenizer, DefaultTokenizer.apply)

    override def stopWords(path: String): F[Try[Set[String]]] =
      processor(path)(processorFactory.stopWords, (s: Set[String]) => s)

    private def processor[A, P](modelPath: String)(f: String => F[Try[A]], g: A => P): F[Try[P]] =
      F.map(f(modelPath)) {
        case Success(api) => Success(g(api))
        case Failure(e)   => Failure(e)
      }
  }
}
