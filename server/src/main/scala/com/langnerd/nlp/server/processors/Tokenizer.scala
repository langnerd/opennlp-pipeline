package com.langnerd.nlp.server.processors

import com.langnerd.nlp.server.model.Token

trait Tokenizer {
  def tokenize(text: String): List[Token]
}

