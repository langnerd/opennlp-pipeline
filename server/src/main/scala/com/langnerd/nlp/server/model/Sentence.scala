package com.langnerd.nlp.server.model

final case class Sentence(text: String, tokens: List[Token], spans: List[Span])
