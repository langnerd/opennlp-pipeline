package com.langnerd.nlp.server.processors.impl

import com.langnerd.nlp.api.processors.{Tokenizer => ApiWordTokenizer}
import com.langnerd.nlp.server.model.Token
import com.langnerd.nlp.server.processors.Tokenizer

class DefaultTokenizer(api: ApiWordTokenizer) extends Tokenizer {
  override def tokenize(text: String): List[Token] =
    api.tokenize(text).map(Token(_, None, None))
}

object DefaultTokenizer {
  def apply(api: ApiWordTokenizer): DefaultTokenizer = new DefaultTokenizer(api)
}
