package com.langnerd.nlp.server.pipeline

import com.langnerd.nlp.server.processors._

import scala.util.{Failure, Success, Try}

final case class PipelineBuilder private (sentenceDetector: Option[SentenceDetector],
                                          tokenizer: Option[Tokenizer],
                                          tagger: Option[PartOfSpeechTagger],
                                          lemmatizer: Option[Lemmatizer],
                                          spanResolvers: List[SpanResolver],
                                          stopWords: Set[String]) {

  def withLemmatizer(lemmatizer: Lemmatizer): PipelineBuilder =
    this.copy(lemmatizer = Some(lemmatizer))

  def withTagger(tagger: PartOfSpeechTagger): PipelineBuilder =
    this.copy(tagger = Some(tagger))

  def withSentenceDetector(sentenceDetector: SentenceDetector): PipelineBuilder =
    this.copy(sentenceDetector = Some(sentenceDetector))

  def withSpanResolver(spanResolver: SpanResolver): PipelineBuilder =
    this.copy(spanResolvers = this.spanResolvers :+ spanResolver)

  def withTokenizer(tokenizer: Tokenizer): PipelineBuilder =
    this.copy(tokenizer = Some(tokenizer))

  def withStopWords(stopWords: Set[String]): PipelineBuilder =
    this.copy(stopWords = stopWords)

  def build(): Try[Pipeline] = (sentenceDetector, tokenizer, tagger, lemmatizer) match {
    case (Some(sentenceDetector), Some(tokenizer), Some(tagger), Some(lemmatizer)) =>
      Success(new Pipeline(sentenceDetector, tokenizer, tagger, lemmatizer, spanResolvers, stopWords))
    case _ =>
      Failure(new IllegalStateException("One or more of the mandatory processors are missing."))
  }
}

object PipelineBuilder {
  final val Empty = PipelineBuilder(None, None, None, None, List.empty, Set.empty)
}
