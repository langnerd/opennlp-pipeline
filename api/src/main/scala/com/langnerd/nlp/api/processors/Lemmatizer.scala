package com.langnerd.nlp.api.processors

trait Lemmatizer {

  def lemmatize(tokens: List[String], tags: List[String]): List[String]
}
