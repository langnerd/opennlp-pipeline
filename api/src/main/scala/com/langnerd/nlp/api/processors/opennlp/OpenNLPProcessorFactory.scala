package com.langnerd.nlp.api.processors.opennlp

import java.io.InputStream

import cats.effect.{ConcurrentEffect, ContextShift, Resource, Sync}
import com.langnerd.nlp.api.processors._
import com.langnerd.nlp.common.io.FileReader
import _root_.opennlp.tools.lemmatizer.DictionaryLemmatizer
import _root_.opennlp.tools.namefind.{NameFinderME, TokenNameFinderModel}
import _root_.opennlp.tools.postag.{POSModel, POSTaggerME}
import _root_.opennlp.tools.sentdetect.{SentenceDetectorME, SentenceModel}
import _root_.opennlp.tools.tokenize.{TokenizerME, TokenizerModel}

import scala.concurrent.ExecutionContextExecutorService
import scala.io.Source
import scala.util.{Failure, Success, Try}

class OpenNLPProcessorFactory[F[_]](implicit readEC: Resource[F, ExecutionContextExecutorService],
                                    contextShift: ContextShift[F],
                                    F: Sync[F],
                                    ce: ConcurrentEffect[F])
    extends ProcessorFactory[F] {

  override def lemmatizer(modelPath: String): F[Try[Lemmatizer]] =
    processor(modelPath)(is => {
      OpenNLPLemmatizer(new DictionaryLemmatizer(is))
    })

  override def sentenceDetector(modelPath: String): F[Try[SentenceDetector]] =
    processor(modelPath)(is => {
      val model = new SentenceModel(is)
      OpenNLPSentenceDetector(new SentenceDetectorME(model))
    })

  override def spanResolver(modelPath: String): F[Try[SpanResolver]] =
    processor(modelPath)(is => {
      val model = new TokenNameFinderModel(is)
      OpenNLPSpanResolver(new NameFinderME(model))
    })

  override def tagger(modelPath: String): F[Try[PartOfSpeechTagger]] =
    processor(modelPath)(is => {
      val model = new POSModel(is)
      OpenNLPPartOfSpeechTagger(new POSTaggerME(model))
    })

  override def tokenizer(modelPath: String): F[Try[Tokenizer]] =
    processor(modelPath)(is => {
      val model = new TokenizerModel(is)
      OpenNLPTokenizer(new TokenizerME(model))
    })

  override def stopWords(path: String): F[Try[Set[String]]] =
    processor(path)(is => Source.fromInputStream(is).getLines().toSet)

  private def processor[P](modelPath: String)(f: InputStream => P): F[Try[P]] = {
    FileReader.readFile(modelPath).use {
      case Success(is) => F.pure(Success(f(is)))
      case Failure(e)  => F.pure(Failure(e))
    }
  }
}
