package com.langnerd.nlp.api.processors.opennlp

import com.langnerd.nlp.api.processors.PartOfSpeechTagger
import opennlp.tools.postag.POSTagger

class OpenNLPPartOfSpeechTagger(client: POSTagger) extends PartOfSpeechTagger {
  override def tag(tokens: List[String]): List[String] = client.tag(tokens.toArray).toList
}

object OpenNLPPartOfSpeechTagger {
  def apply(client: POSTagger): OpenNLPPartOfSpeechTagger = new OpenNLPPartOfSpeechTagger(client)
}
