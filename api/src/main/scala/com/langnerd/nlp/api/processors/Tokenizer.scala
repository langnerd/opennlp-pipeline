package com.langnerd.nlp.api.processors

trait Tokenizer {

  def tokenize(text: String): List[String]
}
