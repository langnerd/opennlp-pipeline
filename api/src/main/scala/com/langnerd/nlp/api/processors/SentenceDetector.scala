package com.langnerd.nlp.api.processors

trait SentenceDetector {

  def detect(text: String): List[String]
}
