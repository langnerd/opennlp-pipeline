package com.langnerd.nlp.api.processors.opennlp

import com.langnerd.nlp.api.processors.SentenceDetector
import opennlp.tools.sentdetect.{SentenceDetector => ClientSentenceDetector}

class OpenNLPSentenceDetector(client: ClientSentenceDetector) extends SentenceDetector {
  override def detect(text: String): List[String] = client.sentDetect(text).toList
}

object OpenNLPSentenceDetector {
  def apply(client: ClientSentenceDetector): OpenNLPSentenceDetector = new OpenNLPSentenceDetector(client)
}
