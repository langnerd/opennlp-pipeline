package com.langnerd.nlp.api.processors

import com.langnerd.nlp.api.model.Span

trait SpanResolver {

  def resolve(tokens: List[String]): List[Span]
}
