package com.langnerd.nlp.api.processors.opennlp

import com.langnerd.nlp.api.processors.Tokenizer
import opennlp.tools.tokenize.{Tokenizer => ClientTokenizer}

class OpenNLPTokenizer(client: ClientTokenizer) extends Tokenizer {
  override def tokenize(text: String): List[String] = client.tokenize(text).toList
}

object OpenNLPTokenizer {
  def apply(client: ClientTokenizer): OpenNLPTokenizer = new OpenNLPTokenizer(client)
}
