package com.langnerd.nlp.api.processors.opennlp

import com.langnerd.nlp.api.model.Span
import com.langnerd.nlp.api.processors.SpanResolver
import opennlp.tools.namefind.NameFinderME

class OpenNLPSpanResolver(client: NameFinderME) extends SpanResolver {

  override def resolve(tokens: List[String]): List[Span] =
    client.find(tokens.toArray).toList.map(s => Span(s.getStart, s.getEnd, s.getType))
}

object OpenNLPSpanResolver {
  def apply(client: NameFinderME): OpenNLPSpanResolver = new OpenNLPSpanResolver(client)
}
