package com.langnerd.nlp.api.processors

import scala.util.Try

trait ProcessorFactory[F[_]] {

  def lemmatizer(modelPath: String): F[Try[Lemmatizer]]

  def sentenceDetector(modelPath: String): F[Try[SentenceDetector]]

  def spanResolver(modelPath: String): F[Try[SpanResolver]]

  def tagger(modelPath: String): F[Try[PartOfSpeechTagger]]

  def tokenizer(modelPath: String): F[Try[Tokenizer]]

  def stopWords(path: String): F[Try[Set[String]]]
}
