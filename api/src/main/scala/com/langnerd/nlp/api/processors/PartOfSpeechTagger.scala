package com.langnerd.nlp.api.processors

trait PartOfSpeechTagger {

  def tag(tokens: List[String]): List[String]
}
