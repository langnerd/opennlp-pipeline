package com.langnerd.nlp.api.model

case class Span(start: Int, end: Int, kind: String)
