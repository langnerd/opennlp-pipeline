package com.langnerd.nlp.api.processors.opennlp

import com.langnerd.nlp.api.processors.Lemmatizer
import opennlp.tools.lemmatizer.DictionaryLemmatizer

class OpenNLPLemmatizer(client: DictionaryLemmatizer) extends Lemmatizer {
  override def lemmatize(tokens: List[String], tags: List[String]): List[String] =
    client.lemmatize(tokens.toArray, tags.toArray).toList
}

object OpenNLPLemmatizer {
  def apply(client: DictionaryLemmatizer): OpenNLPLemmatizer = new OpenNLPLemmatizer(client)
}
