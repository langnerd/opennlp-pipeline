logLevel := Level.Warn

addSbtPlugin("org.scalameta"   % "sbt-scalafmt"    % "2.0.4")
addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3")
