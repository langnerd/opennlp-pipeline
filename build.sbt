name := "nlp"
organization := "com.langnerd"
version := "0.1"
scalaVersion := "2.13.1"

// PROJECT

lazy val root = project
  .in(file("."))
  .settings(commonSettings)
  .aggregate(
    api,
    common,
    server
  )

lazy val api = project
  .in(file("./api"))
  .settings(commonSettings, libraryDependencies ++= commonDependencies)
  .dependsOn(common)

lazy val common = project
  .in(file("./common"))
  .settings(commonSettings, libraryDependencies ++= commonDependencies)

lazy val server = project
  .in(file("./server"))
  .settings(commonSettings, libraryDependencies ++= commonDependencies)
  .dependsOn(api, common)

// SETTINGS

lazy val commonSettings = Seq(
  scalacOptions ++= compilerOptions,
  resolvers ++= Seq(Resolver.sonatypeRepo("releases"))
) ++ Seq(scalafmtOnCompile := true)

lazy val compilerOptions = Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-encoding",
  "utf8"
)

// DEPENDENCIES

lazy val commonDependencies = Seq(
  "org.typelevel" %% "cats-effect" % "2.0.0",
  "org.scalatest" %% "scalatest" % "3.0.8" % "test"
)

libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.8.0"
libraryDependencies += "org.typelevel"        %% "cats-effect"  % "2.0.0"
libraryDependencies += "co.fs2"               %% "fs2-core"     % "2.0.1"
libraryDependencies += "co.fs2"               %% "fs2-io"       % "2.0.1"
libraryDependencies += "org.apache.opennlp"   % "opennlp-tools" % "1.9.1"
