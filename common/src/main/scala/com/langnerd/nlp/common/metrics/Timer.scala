package com.langnerd.nlp.common.metrics

import cats.effect._
import cats.implicits._
import scala.concurrent.duration.MILLISECONDS

object Timer {

  def measure[F[_], A](fa: F[A])(implicit F: Sync[F], clock: Clock[F]): F[(A, Long)] =
    for {
      start <- clock.monotonic(MILLISECONDS)
      result <- fa
      finish <- clock.monotonic(MILLISECONDS)
    } yield (result, finish - start)
}
