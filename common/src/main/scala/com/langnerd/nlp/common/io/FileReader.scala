package com.langnerd.nlp.common.io

import java.io.InputStream
import java.nio.file.{Files, Paths}

import cats.effect.{Resource, Sync}

import scala.util.{Failure, Success, Try}

object FileReader {
  def readFile[F[_]](path: String)(implicit F: Sync[F]): Resource[F, Try[InputStream]] = {
    Resource.make {
      F.delay(Try(Files.newInputStream(Paths.get(path))) match {
        case Success(io) => if (io != null) {
          Success(io)
        } else {
          Failure(new IllegalStateException("File not found!"))
        }
        case fail @ Failure(_) => fail
      })
    } {
      case Success(io) =>
        F.handleErrorWith(F.delay(io.close()))(_ => F.unit)
      case Failure(_) =>
        F.unit
    }
  }
}
